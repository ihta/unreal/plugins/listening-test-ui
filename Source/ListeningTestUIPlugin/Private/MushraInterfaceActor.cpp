// Fill out your copyright notice in the Description page of Project Settings.


#include "MushraInterfaceActor.h"
#include "MushraInterfaceWidget.h"


AMushraInterfaceActor::AMushraInterfaceActor()
{
	PrimaryActorTick.bCanEverTick = true;

	static ConstructorHelpers::FObjectFinder<UClass> MushraInterfaceBlueprint(TEXT("Class'/ListeningTestUIPlugin/MushraInterfaceBlueprint.MushraInterfaceBlueprint_C'"));
	MushraInterfaceWidgetClass = MushraInterfaceBlueprint.Object;
	MushraInterfaceWidgetComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("Mushra Interface Component"));
	MushraInterfaceWidgetComponent->SetWidgetClass(MushraInterfaceWidgetClass);
}

void AMushraInterfaceActor::BeginPlay()
{
	Super::BeginPlay();
	SetWidgetProperties();
}

void AMushraInterfaceActor::ProcessStartupData(FString Message)
{
	UE_LOG(LogTemp, Warning, TEXT("Message: %s"), *Message);
	SliderAStartValue = FCString::Atof(*Message.Mid(0, 4));
	SliderBStartValue = FCString::Atof(*Message.Mid(4, 4));
	SliderCStartValue = FCString::Atof(*Message.Mid(8, 4));
	ResetSliders = Message.Mid(12, 1).ToBool();
	DiscreteSliders = Message.Mid(13, 1).ToBool();
	int32 attrsize = FCString::Atoi(*Message.Mid(18, 4));
	FString attr = Message.Mid(22, attrsize);
	int32 lvlsize = FCString::Atoi(*Message.Mid(22 + attrsize, 4));
	FString lvl = Message.Mid(22 + attrsize + 4, lvlsize);
	RandomizeLevels = Message.Mid(22 + attrsize + 4 + lvlsize, 1).ToBool();
	HeadTrackingEnabled = Message.Mid(22 + attrsize + 4 + lvlsize + 1).ToBool();
	FString delimiter = TEXT(",");
	attr.ParseIntoArray(Attributes, *delimiter);
	lvl.ParseIntoArray(LevelNames, *delimiter);
	SetWidgetProperties();
	UE_LOG(LogTemp, Log, TEXT("Startup data processed. Values set."));
}

void AMushraInterfaceActor::SetWidgetProperties()
{
	MushraInterfaceWidgetComponent->SetDrawSize(FVector2D(1920, 1080));
	MushraInterfaceWidgetComponent->SetWorldScale3D(FVector(0.3, 0.3, 0.3));
	MushraInterfaceWidgetComponent->SetBlendMode(EWidgetBlendMode::Transparent);

	UMushraInterfaceWidget* MushraWidget = Cast<UMushraInterfaceWidget>(MushraInterfaceWidgetComponent->GetUserWidgetObject());
	MushraWidget->setStartValues(SliderAStartValue, SliderBStartValue, SliderCStartValue);
	MushraWidget->setSliderReset(ResetSliders);
	MushraWidget->setSliderDiscrete(DiscreteSliders);
	MushraWidget->setAttributes(Attributes);
	if (Attributes.Num() > 0)
	{
		MushraWidget->TextAttribute->SetText(FText::FromString(Attributes[0]));
	}

	if (RandomizeLevels)
	{
		if (LevelNames.Num() > 0)
		{
			UE_LOG(LogTemp, Log, TEXT("Randomizing levels."));
			int32 LastIndex = LevelNames.Num() - 1;
			for (int32 i = 0; i <= LastIndex; i++)
			{
				int32 newindex = FMath::RandRange(i, LastIndex);
				if (i != newindex)
				{
					LevelNames.Swap(i, newindex);
				}
			}
			MushraWidget->setLevelNames(LevelNames);
			MushraWidget->UpdateSublevel();
		}
	}
	else
	{
		MushraWidget->setLevelNames(LevelNames);
	}

	if (HeadTrackingEnabled)
	{
		TcpConnection->EnableHeadTracking();
	}
}
