// Fill out your copyright notice in the Description page of Project Settings.


#include "MushraInterfaceWidget.h"
#include "MushraInterfaceActor.h"

void UMushraInterfaceWidget::NativeConstruct()
{
	Super::NativeConstruct();

	//Bind Events
	ButtonA->OnClicked.AddUniqueDynamic(this, &UMushraInterfaceWidget::ButtonAFeedback);
	ButtonB->OnClicked.AddUniqueDynamic(this, &UMushraInterfaceWidget::ButtonBFeedback);
	ButtonC->OnClicked.AddUniqueDynamic(this, &UMushraInterfaceWidget::ButtonCFeedback);
	ButtonPrevAttribute->OnClicked.AddUniqueDynamic(this, &UMushraInterfaceWidget::ButtonPrevFeedback);
	ButtonNextAttribute->OnClicked.AddUniqueDynamic(this, &UMushraInterfaceWidget::ButtonNextFeedback);
	ButtonFinish->OnClicked.AddUniqueDynamic(this, &UMushraInterfaceWidget::ButtonFinishFeedback);
	ButtonHelp->OnClicked.AddUniqueDynamic(this, &UMushraInterfaceWidget::ButtonHelpFeedback);
	SliderA->OnMouseCaptureEnd.AddUniqueDynamic(this, &UMushraInterfaceWidget::SliderAFeedback);
	SliderB->OnMouseCaptureEnd.AddUniqueDynamic(this, &UMushraInterfaceWidget::SliderBFeedback);
	SliderC->OnMouseCaptureEnd.AddUniqueDynamic(this, &UMushraInterfaceWidget::SliderCFeedback);

	ButtonA->SetBackgroundColor(FLinearColor(0.150129f, 0.494792f, 0.054118f, 1.0f));
	ButtonB->SetBackgroundColor(FLinearColor(1.0f, 1.0f, 1.0f, 1.0f));
	ButtonC->SetBackgroundColor(FLinearColor(1.0f, 1.0f, 1.0f, 1.0f));
	TextAttribute->SetText(FText::FromString(TextAttributeContent));
}

void UMushraInterfaceWidget::UpdateWidget()
{
	
}

void UMushraInterfaceWidget::setStartValues(float A, float B, float C)
{
	SliderAStartValue = A;
	SliderBStartValue = B;
	SliderCStartValue = C;
	SliderA->SetValue(A);
	SliderB->SetValue(B);
	SliderC->SetValue(C);
}

void UMushraInterfaceWidget::setSliderReset(bool reset)
{
	ResetSliders = reset;
}

void UMushraInterfaceWidget::setSliderDiscrete(bool discrete)
{
	DiscreteSliders = discrete;
}

void UMushraInterfaceWidget::setAttributes(TArray<FString> newattr)
{
	Attributes = newattr;
}

void UMushraInterfaceWidget::setLevelNames(TArray<FString> newlvl)
{
	LevelNames = newlvl;
}

void UMushraInterfaceWidget::setLevelState(uint8 lvlst)
{
	LevelState = lvlst;
}

float UMushraInterfaceWidget::getSliderAStartValue()
{
	return SliderAStartValue;
}

float UMushraInterfaceWidget::getSliderBStartValue()
{
	return SliderBStartValue;
}

float UMushraInterfaceWidget::getSliderCStartValue()
{
	return SliderCStartValue;
}

bool UMushraInterfaceWidget::getSliderReset()
{
	return ResetSliders;
}

bool UMushraInterfaceWidget::getSliderDiscrete()
{
	return DiscreteSliders;
}

void UMushraInterfaceWidget::ChangeSublevel(FString LevelName)
{
	int32 ind;
	if (!LevelNames.Find(LevelName, ind))
	{
		UE_LOG(LogTemp, Error, TEXT("Level name not found."));
		return;
	}

	if (ind == LevelState)
	{
		return;
	}

	for (int32 i = 0; i < LevelNames.Num(); i++)
	{
		UGameplayStatics::GetStreamingLevel(GetWorld(), FName(LevelNames[i]))->SetShouldBeVisible(false);
	}

	UGameplayStatics::GetStreamingLevel(GetWorld(), FName(LevelName))->SetShouldBeVisible(true);
}

void UMushraInterfaceWidget::UpdateSublevel()
{
	FLatentActionInfo LatentInfo;
	//unload currently loaded levels
	for (int32 i = 0; i < LevelNames.Num(); i++)
	{
		LatentInfo.UUID = i;
		UGameplayStatics::LoadStreamLevel(this, FName(LevelNames[i]), false, false, LatentInfo);
	}

	UGameplayStatics::GetStreamingLevel(GetWorld(), FName(LevelNames[LevelState]))->SetShouldBeVisible(true);
}

void UMushraInterfaceWidget::ButtonAFeedback()
{
	UE_LOG(LogTemp, Log, TEXT("Button A pressed."));
	ChangeSublevel(LevelNames[0]);
	LevelState = 0;
	ButtonA->SetBackgroundColor(FLinearColor(0.150129f, 0.494792f, 0.054118f, 1.0f));
	ButtonB->SetBackgroundColor(FLinearColor(1.0f, 1.0f, 1.0f, 1.0f));
	ButtonC->SetBackgroundColor(FLinearColor(1.0f, 1.0f, 1.0f, 1.0f));
}

void UMushraInterfaceWidget::ButtonBFeedback()
{
	UE_LOG(LogTemp, Log, TEXT("Button B pressed."));
	ChangeSublevel(LevelNames[1]);
	LevelState = 1;
	ButtonB->SetBackgroundColor(FLinearColor(0.150129f, 0.494792f, 0.054118f, 1.0f));
	ButtonA->SetBackgroundColor(FLinearColor(1.0f, 1.0f, 1.0f, 1.0f));
	ButtonC->SetBackgroundColor(FLinearColor(1.0f, 1.0f, 1.0f, 1.0f));
}

void UMushraInterfaceWidget::ButtonCFeedback()
{
	UE_LOG(LogTemp, Log, TEXT("Button C pressed."));
	ChangeSublevel(LevelNames[2]);
	LevelState = 2;
	ButtonC->SetBackgroundColor(FLinearColor(0.150129f, 0.494792f, 0.054118f, 1.0f));
	ButtonA->SetBackgroundColor(FLinearColor(1.0f, 1.0f, 1.0f, 1.0f));
	ButtonB->SetBackgroundColor(FLinearColor(1.0f, 1.0f, 1.0f, 1.0f));
}

void UMushraInterfaceWidget::ButtonPrevFeedback()
{
	UE_LOG(LogTemp, Log, TEXT("Button Prev pressed."));

	if (Attributes.Num() == 0)
	{
		return;
	}

	if (CurrentAttribute > 0)
	{
		CurrentAttribute--;
		TextAttribute->SetText(FText::FromString(Attributes[CurrentAttribute]));
	}
}

void UMushraInterfaceWidget::ButtonNextFeedback()
{
	UE_LOG(LogTemp, Log, TEXT("Button Next pressed."));

	if (Attributes.Num() == 0)
	{
		return;
	}

	if (CurrentAttribute < Attributes.Num() - 1)
	{
		CurrentAttribute++;
		TextAttribute->SetText(FText::FromString(Attributes[CurrentAttribute]));
	}
}

void UMushraInterfaceWidget::ButtonFinishFeedback()
{
	UE_LOG(LogTemp, Log, TEXT("Button Finish pressed."));
	AActor* foundActor = UGameplayStatics::GetActorOfClass(GetWorld(), AMushraInterfaceActor::StaticClass());
	AMushraInterfaceActor* InterfaceActor = Cast<AMushraInterfaceActor>(foundActor);
	if (!InterfaceActor)
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to find Tcp Connection"));
		return;
	}
	ATcpConnectionActor* TcpConnection = InterfaceActor->GetTcpConnection();
	TArray<uint8> Message = TArray<uint8>({ TcpConnection->Conv_StringToBytes(TEXT("015")) });
	if (TcpConnection->isHeadTrackingEnabled())
	{
		TcpConnection->WriteToFile();
		Message.Append(TcpConnection->Conv_StringToBytes(TEXT("1")));
		TcpConnection->DisableHeadTracking();
	}
	else
	{
		Message.Append(TcpConnection->Conv_StringToBytes(TEXT("0")));
	}
	TcpConnection->SendMessage(Message);
}

void UMushraInterfaceWidget::ButtonHelpFeedback()
{
	UE_LOG(LogTemp, Log, TEXT("Button Help pressed."));
}

void UMushraInterfaceWidget::SliderAFeedback()
{
	UE_LOG(LogTemp, Log, TEXT("Slider A value changed: %f"), SliderA->GetValue());
	AActor* foundActor = UGameplayStatics::GetActorOfClass(GetWorld(), AMushraInterfaceActor::StaticClass());
	AMushraInterfaceActor* InterfaceActor = Cast<AMushraInterfaceActor>(foundActor);
	if (!InterfaceActor)
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to find Tcp Connection"));
		return;
	}
	ATcpConnectionActor* TcpConnection = InterfaceActor->GetTcpConnection();

	float value = SliderA->GetValue();
	TArray<uint8> Message = TArray<uint8>({ TcpConnection->Conv_StringToBytes(TEXT("002")) });
	Message.Append(TcpConnection->Conv_StringToBytes(TEXT("A")));
	switch (LevelState)
	{
	case 0:
		Message.Append(TcpConnection->Conv_StringToBytes(TEXT("A")));
		break;
	case 1:
		Message.Append(TcpConnection->Conv_StringToBytes(TEXT("B")));
		break;
	case 2:
		Message.Append(TcpConnection->Conv_StringToBytes(TEXT("C")));
		break;
	}
	TArray<uint8> CurrentLevel = TcpConnection->Conv_StringToBytes(LevelNames[LevelState]);
	FString CurrentLevelNum = FString::FromInt(CurrentLevel.Num());
	FString zerofill;
	switch (CurrentLevelNum.Len())
	{
	case 1:
		zerofill = "000";
		break;
	case 2:
		zerofill = "00";
		break;
	case 3:
		zerofill = "0";
		break;
	case 4:
		zerofill = "";
		break;
	default:
		break;
	}
	CurrentLevelNum = zerofill + CurrentLevelNum;
	Message.Append(TcpConnection->Conv_StringToBytes(CurrentLevelNum));
	Message.Append(CurrentLevel);
	TArray<uint8> CurrentAttr = TcpConnection->Conv_StringToBytes(Attributes[CurrentAttribute]);
	FString CurrentAttrNum = FString::FromInt(CurrentAttr.Num());
	switch (CurrentAttrNum.Len())
	{
	case 1:
		zerofill = "000";
		break;
	case 2:
		zerofill = "00";
		break;
	case 3:
		zerofill = "0";
		break;
	case 4:
		zerofill = "";
		break;
	default:
		break;
	}
	CurrentAttrNum = zerofill + CurrentAttrNum;
	Message.Append(TcpConnection->Conv_StringToBytes(CurrentAttrNum));
	Message.Append(CurrentAttr);
	Message.Append(TcpConnection->Conv_FloatToBytes(SliderAStartValue));
	Message.Append(TcpConnection->Conv_FloatToBytes(value));
	Message.Append(TcpConnection->Conv_StringToBytes(DiscreteSliders ? TEXT("1") : TEXT("0")));
	Message.Append(TcpConnection->Conv_StringToBytes(ResetSliders ? TEXT("1") : TEXT("0")));
	
	TcpConnection->SendMessage(Message);
}

void UMushraInterfaceWidget::SliderBFeedback()
{
	UE_LOG(LogTemp, Log, TEXT("Slider B value changed: %f"), SliderB->GetValue());
	AActor* foundActor = UGameplayStatics::GetActorOfClass(GetWorld(), AMushraInterfaceActor::StaticClass());
	AMushraInterfaceActor* InterfaceActor = Cast<AMushraInterfaceActor>(foundActor);
	if (!InterfaceActor)
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to find Tcp Connection"));
		return;
	}
	ATcpConnectionActor* TcpConnection = InterfaceActor->GetTcpConnection();

	float value = SliderB->GetValue();
	TArray<uint8> Message = TArray<uint8>({ TcpConnection->Conv_StringToBytes(TEXT("002")) });
	Message.Append(TcpConnection->Conv_StringToBytes(TEXT("B")));
	switch (LevelState)
	{
	case 0:
		Message.Append(TcpConnection->Conv_StringToBytes(TEXT("A")));
		break;
	case 1:
		Message.Append(TcpConnection->Conv_StringToBytes(TEXT("B")));
		break;
	case 2:
		Message.Append(TcpConnection->Conv_StringToBytes(TEXT("C")));
		break;
	}
	TArray<uint8> CurrentLevel = TcpConnection->Conv_StringToBytes(LevelNames[LevelState]);
	FString CurrentLevelNum = FString::FromInt(CurrentLevel.Num());
	FString zerofill;
	switch (CurrentLevelNum.Len())
	{
	case 1:
		zerofill = "000";
		break;
	case 2:
		zerofill = "00";
		break;
	case 3:
		zerofill = "0";
		break;
	case 4:
		zerofill = "";
		break;
	default:
		break;
	}
	CurrentLevelNum = zerofill + CurrentLevelNum;
	Message.Append(TcpConnection->Conv_StringToBytes(CurrentLevelNum));
	Message.Append(CurrentLevel);
	TArray<uint8> CurrentAttr = TcpConnection->Conv_StringToBytes(Attributes[CurrentAttribute]);
	FString CurrentAttrNum = FString::FromInt(CurrentAttr.Num());
	switch (CurrentAttrNum.Len())
	{
	case 1:
		zerofill = "000";
		break;
	case 2:
		zerofill = "00";
		break;
	case 3:
		zerofill = "0";
		break;
	case 4:
		zerofill = "";
		break;
	default:
		break;
	}
	CurrentAttrNum = zerofill + CurrentAttrNum;
	Message.Append(TcpConnection->Conv_StringToBytes(CurrentAttrNum));
	Message.Append(CurrentAttr);
	Message.Append(TcpConnection->Conv_FloatToBytes(SliderBStartValue));
	Message.Append(TcpConnection->Conv_FloatToBytes(value));
	Message.Append(TcpConnection->Conv_StringToBytes(DiscreteSliders ? TEXT("1") : TEXT("0")));
	Message.Append(TcpConnection->Conv_StringToBytes(ResetSliders ? TEXT("1") : TEXT("0")));

	TcpConnection->SendMessage(Message);
}

void UMushraInterfaceWidget::SliderCFeedback()
{
	UE_LOG(LogTemp, Log, TEXT("Slider C value changed: %f"), SliderC->GetValue());
	AActor* foundActor = UGameplayStatics::GetActorOfClass(GetWorld(), AMushraInterfaceActor::StaticClass());
	AMushraInterfaceActor* InterfaceActor = Cast<AMushraInterfaceActor>(foundActor);
	if (!InterfaceActor)
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed to find Tcp Connection"));
		return;
	}
	ATcpConnectionActor* TcpConnection = InterfaceActor->GetTcpConnection();

	float value = SliderC->GetValue();
	TArray<uint8> Message = TArray<uint8>({ TcpConnection->Conv_StringToBytes(TEXT("002")) });
	Message.Append(TcpConnection->Conv_StringToBytes(TEXT("C")));
	switch (LevelState)
	{
	case 0:
		Message.Append(TcpConnection->Conv_StringToBytes(TEXT("A")));
		break;
	case 1:
		Message.Append(TcpConnection->Conv_StringToBytes(TEXT("B")));
		break;
	case 2:
		Message.Append(TcpConnection->Conv_StringToBytes(TEXT("C")));
		break;
	}
	TArray<uint8> CurrentLevel = TcpConnection->Conv_StringToBytes(LevelNames[LevelState]);
	FString CurrentLevelNum = FString::FromInt(CurrentLevel.Num());
	FString zerofill;
	switch (CurrentLevelNum.Len())
	{
	case 1:
		zerofill = "000";
		break;
	case 2:
		zerofill = "00";
		break;
	case 3:
		zerofill = "0";
		break;
	case 4:
		zerofill = "";
		break;
	default:
		break;
	}
	CurrentLevelNum = zerofill + CurrentLevelNum;
	Message.Append(TcpConnection->Conv_StringToBytes(CurrentLevelNum));
	Message.Append(CurrentLevel);
	TArray<uint8> CurrentAttr = TcpConnection->Conv_StringToBytes(Attributes[CurrentAttribute]);
	FString CurrentAttrNum = FString::FromInt(CurrentAttr.Num());
	switch (CurrentAttrNum.Len())
	{
	case 1:
		zerofill = "000";
		break;
	case 2:
		zerofill = "00";
		break;
	case 3:
		zerofill = "0";
		break;
	case 4:
		zerofill = "";
		break;
	default:
		break;
	}
	CurrentAttrNum = zerofill + CurrentAttrNum;
	Message.Append(TcpConnection->Conv_StringToBytes(CurrentAttrNum));
	Message.Append(CurrentAttr);
	Message.Append(TcpConnection->Conv_FloatToBytes(SliderCStartValue));
	Message.Append(TcpConnection->Conv_FloatToBytes(value));
	Message.Append(TcpConnection->Conv_StringToBytes(DiscreteSliders ? TEXT("1") : TEXT("0")));
	Message.Append(TcpConnection->Conv_StringToBytes(ResetSliders ? TEXT("1") : TEXT("0")));

	TcpConnection->SendMessage(Message);
}