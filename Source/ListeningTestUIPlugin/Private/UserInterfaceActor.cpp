// Fill out your copyright notice in the Description page of Project Settings.


#include "UserInterfaceActor.h"

// Sets default values
AUserInterfaceActor::AUserInterfaceActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	FollowPlayer = false;
}

// Called when the game starts or when spawned
void AUserInterfaceActor::BeginPlay()
{
	Super::BeginPlay();
	AActor* foundActor = UGameplayStatics::GetActorOfClass(GetWorld(), ATcpConnectionActor::StaticClass());
	if (!foundActor)
	{
		UE_LOG(LogTemp, Error, TEXT("Failed to find Tcp Connection."));
		return;
	}
	TcpConnection = Cast<ATcpConnectionActor>(foundActor);

	if (FollowPlayer)
	{
		AActor* FoundPawn = UGameplayStatics::GetActorOfClass(GetWorld(), AVirtualRealityPawn::StaticClass());
		if (!FoundPawn)
		{
			UE_LOG(LogTemp, Error, TEXT("Failed to find Virtual Reality Pawn."));
			return;
		}
		PlayerPawnHead = Cast<AVirtualRealityPawn>(FoundPawn)->Head;
	}
}

// Called every frame
void AUserInterfaceActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (FollowPlayer)
	{
		UpdatePosition();
	}
}

void AUserInterfaceActor::ProcessStartupData(FString Message)
{

}

ATcpConnectionActor* AUserInterfaceActor::GetTcpConnection()
{
	return TcpConnection;
}

void AUserInterfaceActor::UpdatePosition()
{
	if (!PlayerPawnHead)
	{
		UE_LOG(LogTemp, Error, TEXT("Error: Virtual Reality Pawn Head is NULL!"));
		return;
	}
	FVector PlayerForwardVector = 400 * PlayerPawnHead->GetForwardVector();
	FVector PlayerPosition = PlayerPawnHead->K2_GetComponentLocation();
	FRotator PlayerRotation = PlayerPawnHead->K2_GetComponentRotation();
	FRotator WidgetRotation;
	WidgetRotation.Roll = -PlayerRotation.Roll;
	WidgetRotation.Pitch = -PlayerRotation.Pitch;
	WidgetRotation.Yaw = PlayerRotation.Yaw + 180.0;
	this->SetActorLocation(PlayerForwardVector + PlayerPosition);
	this->SetActorRotation(WidgetRotation);
}

void AUserInterfaceActor::ChangeVisibility()
{
	if (IsHidden())
	{
		UE_LOG(LogTemp, Log, TEXT("Changing UI to: visible."));
		SetActorHiddenInGame(false);
	}
	else
	{
		UE_LOG(LogTemp, Log, TEXT("Changing UI to: hidden."));
		SetActorHiddenInGame(true);
	}
}