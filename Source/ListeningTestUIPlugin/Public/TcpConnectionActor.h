// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/GameplayStatics.h"
#include "TcpSocketConnection.h"
#include "TimerManager.h"
#include "TcpConnectionActor.generated.h"

/**
 * Actor that handles communication with TCP server. Sends and receives messages.
 * Specific processing of messages is handled by user interface actors
 */
UCLASS()
class LISTENINGTESTUIPLUGIN_API ATcpConnectionActor : public ATcpSocketConnection
{
	GENERATED_BODY()
	

public:
	//Sets default properties
	ATcpConnectionActor();
	//Called on game start
	virtual void BeginPlay() override;


// ****************************
// **** Callback Functions ****
// ****************************
	UFUNCTION() void OnConnected(int32 ConnectionID);
	UFUNCTION() void OnDisconnected(int32 ConnectionID);
	UFUNCTION() void OnMessageReceived(int32 ConnectionID, TArray<uint8>& Message);
	UFUNCTION() void ProcessMessage(TArray<uint8>& Message);
	UFUNCTION() void SendMessage(TArray<uint8> Message);

// *****************************
// **** Blueprint Functions ****
// *****************************
	UFUNCTION(BlueprintCallable, Category = "Server") void ConnectToServer();
	UFUNCTION(BlueprintCallable, Category = "Server|Conversion") FString ConvBytes2String(TArray<uint8> Bytes);
	UFUNCTION(BlueprintCallable, Category = "Head Tracking") void EnableHeadTracking();
	UFUNCTION(BlueprintCallable, Category = "Head Tracking") void DisableHeadTracking();
	UFUNCTION(BlueprintCallable, Category = "Head Tracking") bool isHeadTrackingEnabled();
	UFUNCTION(BlueprintCallable, Category = "Head Tracking") void WriteToFile();

// ********************
// **** Connection ****
// ********************
	UPROPERTY(EditAnywhere, Category = "Server") FString IPAddress;
	UPROPERTY(EditAnywhere, Category = "Server") int32 PortNumber;
	UPROPERTY(EditAnywhere, Category = "Server") bool ConnectOnStartup;
	UPROPERTY(EditAnywhere, Category = "Server") int32 ServerMode;

// ***********************
// **** Head Tracking ****
// ***********************
	UPROPERTY(EditAnywhere, Category = "Head Tracking") float HeadTrackingSpeed;
	UPROPERTY(EditAnywhere, Category = "Head Tracking") FString HeadTrackingFileName;
	

protected:
	UPROPERTY() int32 ConnectionIDServer;
	void CollectHeadTrackingData();
	void CollectPlayerPosition();

	FTimerHandle TimerHandle;
	bool HeadTrackingEnabled;
	TArray<FVector> HeadTrackingPositions;
	TArray<FRotator> HeadTrackingRotations;
};
