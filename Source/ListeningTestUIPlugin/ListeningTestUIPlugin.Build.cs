// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class ListeningTestUIPlugin : ModuleRules
{
	public ListeningTestUIPlugin(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
		
		PublicIncludePaths.AddRange(
			new string[] {
				// ... add public include paths required here ...
				"TcpSocketPlugin"
			}
			);
				
		
		PrivateIncludePaths.AddRange(
			new string[] {
				// ... add other private include paths required here ...
				"TcpSocketPlugin"
			}
			);
			
		
		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
				"TcpSocketPlugin",
				"UMG",
				"RWTHVRToolkit",
				"HeadMountedDisplay"
				// ... add other public dependencies that you statically link with here ...
			}
			);
			
		
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				"CoreUObject",
				"Engine",
				"Slate",
				"SlateCore",
				"TcpSocketPlugin",
				"UMG",
				"Slate",
				"SlateCore",
				"HeadMountedDisplay"
				// ... add private dependencies that you statically link with here ...	
			}
			);
		
		
		DynamicallyLoadedModuleNames.AddRange(
			new string[]
			{
				// ... add any modules that your module loads dynamically here ...
			}
			);
	}
}
