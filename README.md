# ListeningTestUIPlugin
## Overview
This plugin creates an interface between Unreal Engine and a TCP server using CodeSpartan's [TcpSocketPlugin](https://github.com/CodeSpartan/UE4TcpSocketPlugin) in order to collect data from VR listening experiments. This interface allows UI widgets to send data to e.g. **Matlab** for automatic processing as well as collect head tracking data from HMDs.

## User interfaces
In order to display widgets in 3D space, they are attached to actors which are placed in the world. Currently the only widget is a [mushra-type](https://de.wikipedia.org/wiki/Multi-Stimulus_Test_with_Hidden_Reference_and_Anchor) interface. This widget allows participants to switch between different levels and rate different attributes using sliders. Data gathered from sliders is automatically send to server.

## Messaging
Messages are formatted using a header/body structure, e.g.:
```
Header: 0 0 0 18 0 0 2    Body: 65 73 72 84 65 80 97 114 107 83 117 109 109 101 114 
```
With the header containing the length of the message (4 bytes) as well as an op-code detailing the contents of the message (3 bytes) and the body containing the message itself. Numbers are generally converted into strings before being sent, meaning every character is represented by its ASCII value (1 Byte).
### Op-Codes:
- **001** Ping (for testing purposes)
- **002** Slider Data, content: SliderNr, LevelState, CurrentLevel, CurrentAttribute, SliderStartValue, Value, SliderDiscrete, ResetOnAttributeChange, TimeSec  
- **003** Startup Data, content: SliderAStartValue, SliderBStartValue, SliderCStartValue, ResetSliders, DiscreteSliders, Attributes, HeadTrackingEnabled, LevelNames, RandomizeLevels
- **011** No HMD detected
- **015** Experiment finished, data will be processed 

## Usage
Drop **TcpConnectionActor** into your level and set ip address and port number of your server in the actor's detail panel. When connection to a server is successfully established, data will be sent and information is displayed in UE's output log. In order to display UI, drop **MushraInterfaceActor** into your level.
